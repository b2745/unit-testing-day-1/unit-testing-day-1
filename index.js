const express = require('express');
const app = express();
const port = 5000; 

app.use(express.json());

const newUser = {
    firstName: 'John',
    lastName: 'Dela Cruz',
    age: 18,
    contactNumber: '09123456789',
    batchNumber: 151,
    email: 'john.delacruze@gmail.com',
    password: 'sixteencharacters'
}

module.exports = { 
    newUser: newUser
}

app.listen(port,(req,res) => {
    console.log(`App is running on localhostport ${port}`);
})
