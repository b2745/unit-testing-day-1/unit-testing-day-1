const { assert } = require ('chai');
const { newUser } = require ('../index.js');


describe('Test newUser object', () => {
	it('Assert newUser type is object', () => {
		assert.equal(typeof(newUser), 'object')
	});
	it('Assert newUser.email is type of string',()=>{
		assert.equal(typeof(newUser.email), 'string')
	}); 
	it('Assert newUser.email is is not undefined',()=>{
		assert.notEqual(typeof(newUser.email), 'undefined')
	}); 
	it('Assert newUser.password is type of string',()=>{
		assert.equal(typeof(newUser.password), 'string')
	}); 
	it('Assert newUser.password is 16 characters long',()=>{
		assert.equal(typeof(newUser.password.length), 'number')
	}); 
	it('Assert newUser.firstName is type of string',()=>{
		assert.equal(typeof(newUser.firstName), 'string')
	});
	it('Assert newUser.lastName is type of string',()=>{
		assert.equal(typeof(newUser.lastName), 'string')
	});
	it('Assert newUser.firstName is not undefined',()=>{
		assert.notEqual(typeof(newUser.firstName), 'undefined')
	}); 
	it('Assert newUser.lastName is not undefined',()=>{
		assert.notEqual(typeof(newUser.lastName), 'undefined')
	}); 
	it('Assert newUser.age is at atLeast 18',()=>{
		assert.equal(typeof(newUser.age), 'number')
	}); 
	it('Assert newUser.age is a number',()=>{
		assert.equal(typeof(newUser.age), 'number')
	}); 
	it('Assert newUser.contactNumber is a string',()=>{
		assert.equal(typeof(newUser.contactNumber), 'string')
	}); 
	it('Assert newUser.batchNumber type is a number',()=>{
		assert.equal(typeof(newUser.batchNumber), 'number')
	}); 
	it('Assert newUser.batchNumber is not undefined',()=>{
		assert.notEqual(typeof(newUser.batchNumber), 'undefined')
	}); 
	
});